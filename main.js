//take care of partner image preview

document.querySelector("select[name='partner']").onchange = e => {
    let h = document.querySelector("select[name='partner']");
    document.querySelector("#partner").src = "https://codeberg.org/lustlion/arcaea-skin/raw/branch/master/CHARACTER/SPRITE/"+h.value+".png";
    document.querySelector("#partnerIcon").src = "https://codeberg.org/lustlion/arcaea-skin/raw/branch/master/CHARACTER/ICON/"+h.value+"_icon.png";
};

document.querySelector("select[name='background']").onchange = e => {
    let h = document.querySelector("select[name='background']");
    document.querySelector("#background").src = "https://codeberg.org/lustlion/arcaea-skin/raw/branch/master/BACKGROUND/"+h.value+".jpg";
};

var zipReady = false;
var zip = new JSZip();

function loadZip() {
    fetch('/example.zip')
    .then(response => {
        zip.loadAsync(response.blob()).then(() => {
            zipReady = true;
            document.querySelector("#zipready").innerHTML = "ready!";
        });
    });
}

function addFileToZip(url, path) {
    //affected by cors (DUH but i havent noticed this since i was using img tags up until. yk now)
    //so like - make sure everythings hosted on the same. . . host
    //:(
    fetch(url)
    .then(response => {
        zip.file(path, response.blob());
    });
}

function downloadZipAsOsk() {
    zip.generateAsync({type:"blob"})
    .then(function(content) {
        // see FileSaver.js
        saveAs(content, "arcaea.osk");
    });
}

loadZip();